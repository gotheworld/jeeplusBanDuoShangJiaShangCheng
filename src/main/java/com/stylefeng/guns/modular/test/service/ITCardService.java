package com.stylefeng.guns.modular.test.service;

import com.stylefeng.guns.persistence.test.model.TCard;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zscat
 * @since 2017-05-22
 */
public interface ITCardService extends IService<TCard> {
	
}
