package com.stylefeng.guns.modular.shop.service;

import com.stylefeng.guns.persistence.shop.model.TFloor;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品类型表 服务类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface ITFloorService extends IService<TFloor> {
	
}
