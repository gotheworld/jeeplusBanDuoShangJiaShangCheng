/**
 * 初始化部门详情对话框
 */
var TArticleInfoDlg = {
    tFloorInfoData : {},
    zTreeInstance : null
};

/**
 * 清除数据
 */
TArticleInfoDlg.clearData = function() {
    this.tFloorInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TArticleInfoDlg.set = function(key, val) {
    this.tFloorInfoData[key] = (typeof value == "undefined") ? $("#" + key).val() : value;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TArticleInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TArticleInfoDlg.close = function() {
    parent.layer.close(window.parent.TArticle.layerIndex);
}


/**
 * 收集数据
 */
TArticleInfoDlg.collectData = function() {
    this.set('id').set('name').set('title').set('advimg');
}

/**
 * 提交添加部门
 */
TArticleInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tFloor/add", function(data){
        Feng.success("添加成功!");
        window.parent.TArticle.table.refresh();
        TArticleInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tFloorInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TArticleInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tFloor/update", function(data){
        Feng.success("修改成功!");
        window.parent.TArticle.table.refresh();
        TArticleInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tFloorInfoData);
    ajax.start();
}

function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "parentTArticleMenu" || $(
            event.target).parents("#parentTArticleMenu").length > 0)) {
        TArticleInfoDlg.hideTArticleSelectTree();
    }
}
